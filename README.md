This project aims to provide an application that shows world without details in VR view provided by Cardboard SDK. As an image real time camera feed is being used. To archieve this OpenGL ES, OpenCV and camera 2 are being used. 

![Application overview](.readme_image.jpg)

To get this project up and running should not be too hard. It basically requires few things which are:

1) Downloading Android studio (version 3.6 or newer is required)
2) Following any compilation errors given by Android Studio if there is any

Errors occuring in step 2 might be something to do with used environment and its requirements/settings
